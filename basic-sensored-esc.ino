
#define hall1 1 //Int3
#define hall2 0 //Int2
#define hall3 2 //Int1
int stepstate = 1;
bool hall1state;
bool hall2state;
bool hall3state;
int currentValue1;
int currentValue2;
int currentValue3;


void ExInt3() {
  hall1state = digitalRead(hall1);
  NextStep();
}

void ExInt2() {
  hall2state = digitalRead(hall2);
  NextStep();
}

void ExInt1() {
  hall3state = digitalRead(hall3);
  NextStep();
}

void NextStep() {
//Serial.print("\r\n");
//Serial.print(stepstate);
  if ((hall1state == 1) && (hall2state == 0) && (hall3state == 0)) {
    stepstate = 1;
  }
  if ((hall1state == 1) && (hall2state == 0) && (hall3state == 1)) {
    stepstate = 2;
  }
  if ((hall1state == 0) && (hall2state == 0) && (hall3state == 1)) {
    stepstate = 3;
  }
  if ((hall1state == 0) && (hall2state == 1) && (hall3state == 1)) {
    stepstate = 4;
  }
  if ((hall1state == 0) && (hall2state == 1) && (hall3state == 0)) {
    stepstate = 5;
  }
  if ((hall1state == 1) && (hall2state == 1) && (hall3state == 0)) {
    stepstate = 6;
  }  
  if ((hall1state == 0) && (hall2state == 0) && (hall3state == 0)) {
    PORTB = 0b01011000;
  }
//Serial.print("\nhall1 ");
//Serial.print(hall1state);
//Serial.print("\nhall2 ");
//Serial.print(hall2state);
//Serial.print("\nHall3 ");
//Serial.print(hall3state);
//Serial.print("\n");
//Serial.print(stepstate);
}

void setup()
{
//Serial.begin(115200);

  // Reading and setting the first stepstate so the motor can move after powering up arduino without first moving the motor
  hall1state = digitalRead(hall1);
  hall2state = digitalRead(hall2);
  hall3state = digitalRead(hall3);
  NextStep();

  DDRB = 0b01111110; //set pins 10, 9, 8, 14, 16, 15 as outputs
  PORTB = 0b01110000; //set pins 10, 9, 8 HIGH and 14, 16, 15 LOW (motor off)

  attachInterrupt(digitalPinToInterrupt(1), ExInt3, CHANGE);
  attachInterrupt(digitalPinToInterrupt(0), ExInt2, CHANGE);
  attachInterrupt(digitalPinToInterrupt(2), ExInt1, CHANGE);

  hall1state = digitalRead(hall1);
  hall2state = digitalRead(hall2);
  hall3state = digitalRead(hall3);
}

void loop() {
    //DDRB=0b00000000; // X, 10, 9, 8, 14, 16, 15, X
  switch (stepstate) {
    case 1:
      PORTB = 0b01101000;
      break;
    case 2:
      PORTB = 0b01011000;
      break;
    case 3:
      PORTB = 0b01010010;
      break;
    case 4:
      PORTB = 0b00110010;
      break;
    case 5:
      PORTB = 0b00110100;
      break;
    case 6:
      PORTB = 0b01100100;
    break;
  }
  //}
}
