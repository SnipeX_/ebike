// rp-mcp3008-v2.c		basic program listing analogue values

// 26-May-2018 JST Lawrence V1.0 Initial program

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wiringPi.h>
#include <signal.h>

#include <mcp3004.h>
#include <wiringPiSPI.h>

#define BASE 100
#define SPI_CHAN 0

// Foreground colours
char *BlackFG =  "\033[0;30m";
char *RedFG =    "\033[0;31m";
char *GreenFG =  "\033[0;32m";
char *YellowFG = "\033[0;33m";
char *BlueFG =   "\033[0;34m";
char *MagentaFG ="\033[0;35m";
char *CyanFG =   "\033[0;36m";
char *WhiteFG =  "\033[0;37m";
// Background colours
char *BlackBG =  "\033[0;40m";
char *RedBG =    "\033[0;41m";
char *GreenBG =  "\033[0;42m";
char *YellowBG = "\033[0;43m";
char *BlueBG =   "\033[0;44m";
char *MagentaBG ="\033[0;45m";
char *CyanBG =   "\033[0;46m";
char *WhiteBG =  "\033[0;47m";
// Reset to default
char *Default =  "\033[0m";
// Move cursor
char *UpLines =  "\033[%dA"; // Usage: printf(UpLines,number_of_lines);
char *DownLines ="\033[%dB"; // Usage: printf(DownLines,number_of_lines);
char *HideCursor ="\033[?25l";
char *ShowCursor ="\033[?25h";

int i;

// Function to trap <CTRL>C
void CatchCTRLC() {
    printf("%s", ShowCursor);
    printf(DownLines, 8);

// Entirely optional - insert code here to tidy up before exiting...

    exit(0);
}

long map(long x, long in_min, long in_max, long out_min, long out_max) {
  return (x - in_min) * (out_max - out_min + 1) / (in_max - in_min + 1) + out_min;
}

int main (int argc, char *argv[]) {
    int j, result, voltage;
    unsigned char data[3] = {1};

    double Correction[8] = {1,1,1,1,1,1,1,1};

    // Set up to catch <CTRL>C - should be using sigaction really!
    signal(SIGINT, CatchCTRLC);

    printf(HideCursor);
//  printf("wiringPiSPISetup RC=%d\n",wiringPiSPISetup(0,500000));
//  wiringPiSetupGpio();
    mcp3004Setup(BASE, SPI_CHAN);


    char *adcname[8];
        adcname[0] = "  Pot Voltage: ";
        adcname[1] = "   Motor Temp: ";
        adcname[2] = "  MOSFET Temp: ";
        adcname[3] = "         ADC3: ";
        adcname[4] = "         ADC4: ";
        adcname[5] = "         ADC5: ";
        adcname[6] = "Rotor Voltage: ";
        adcname[7] = "  ESC Voltage: ";

    int maprange0[8] = {0};
        // maprange0[0] = 0;
        // maprange0[1] = 0;
        // maprange0[2] = 0;
        // maprange0[3] = 0;
        // maprange0[4] = 0;
        // maprange0[5] = 0;
        // maprange0[6] = 0;
        // maprange0[7] = 0;

    int maprange[8] = {3300};
        // maprange[0] = 3300;
        // maprange[1] = 3300;
        // maprange[2] = 3300;
        // maprange[3] = 3300;
        // maprange[4] = 3300;
        // maprange[5] = 3300;
        // maprange[6] = 3300;
        maprange[7] = 21900;

//Temperature sensor setup
//These values are in the datasheet of used thermistor
#include <math.h>
float RT0_1, B_1, R_1, VCC, RT, VR, ln, TX, T0, VRT;
RT0_1 = 2200; //Resistance of thermistor
B_1   = 3930; //Beta of thermistor
R_1   = 6800; //R=1,5KΩ
VCC   = 3.3;  //Supply voltage
T0    = 25 + 273.15;  //Temperature T0 from datasheet, conversion from Celsius to kelvin

void bar(int x, int min_value, int max_value) {
    // Map min_value-max_value to 0-40
    x=map(x, min_value, max_value,0,40);
    printf("[%s", GreenFG);

    for(j = 1; j <= x; j++) {
        if(j > 30) printf("%s", YellowFG);
        if(j > 36) printf("%s", RedFG);
        printf("#");
    }

    // Overwite a previously longer line
    for(j = x+1; j <= 40; j++) printf(" ");
    printf("%s]\n", WhiteFG);
}


// Do until CTRL-C
while(1) {
    for(i = 0; i < 8; i++) {
        // Read analogue value
        result = analogRead(BASE+i)*Correction[i];

        if (i==1) {
            VRT = result; //printf("Result: %d\n", result);                         //Acquisition analog value of VRT
            VRT = (VCC / 1023) * (1023 - VRT); //printf("VRT V: %.2f\n ", VRT);     //VRT = (3.3 / 1023) * VRT; //Conversion to voltage
            VR = VCC - VRT;
            RT = VRT / (VR / R_1); //printf("Thermistor R: %.2f\n ", RT);   //Resistance of RT
            ln = log(RT / RT0_1);
            TX = (1 / ((ln / B_1) + (1 / T0)));    //Temperature from thermistor
            TX = TX - 273.15;                      //Conversion to Celsius
            printf("%s %.2f°C	", adcname[i], TX);
            bar(TX, 0, 90);
        }

        // Print out rubric
        voltage = map(result, 0, 1023, maprange0[i], maprange[i]);

        if (i != 1) {
            printf("%s %.2d.%.3dV ", adcname[i], voltage / 1000, voltage % 1000);
            bar(result, 0, 1023);
        }

    }

    // Move cursor back to top of output
    printf("\r           \n\r\n\r");
    printf(UpLines, 10); //8
    // sleep for 250ms
    usleep(250000);
}
} // main loop end

